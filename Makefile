CC := g++
CXXFLAGS := -Wall -lncurses

SRCFILES := $(wildcard src/*.cpp)

all: $(SRCFILES:src/%.cpp=obj/%.o)
	$(CC) obj/*.o -o bin/out $(CXXFLAGS)

obj/%.o : src/%.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@ -I./inc 

.PHONY: clean
clean: 
	rm -rf obj/*
	rm -rf bin/*

game:
	bin/out
