//Classe utilizada para carregar os arquivos de mapa e menu na tela

#ifndef MAP_HPP
#define MAP_HPP

#include "gameobject.hpp"
#include <string>

using namespace std;

class Map : public GameObject{
public:
	Map();
	~Map();

	void displayMap(WINDOW* current_win);
	void displayMenu(WINDOW* current_win);

};

#endif