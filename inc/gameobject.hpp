//Classe pai principal e abstrata 

#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

#include <ncurses.h>

class GameObject{

protected:
	int pos_x;
	int pos_y;
	char sprite;
	WINDOW* current_win;

public:
	GameObject();
	~GameObject();
	
};

#endif
