//Classe utilizada para criar e definir movimentos do jogador

#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "gameobject.hpp"
#include "draw.hpp"

class Player : public GameObject{

private:
	bool alive;
	int score;
	bool winner;
	int life;

public:
	Player();
	~Player();
	Player(WINDOW* win, int y, int x, char c);
	
	bool gameWinner();
	bool gameOver();

	void moveUp();
	void moveDown();
	void moveLeft();
	void moveRight();
	int getMove();
	
	void setDraw();

	void displayInfos();
};

#endif