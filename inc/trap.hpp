//Classe utilizada para criar aleatoriamente caracteres na tela
//que ao serem capturados decrementam a quantidade de vidas do 
//jogador

#ifndef TRAP_HPP
#define TRAP_HPP

#include "gameobject.hpp"

class Trap : public GameObject{

public:
	Trap(WINDOW* win);
	~Trap();

	void displayTraps();
	void destroyTraps();

};

#endif