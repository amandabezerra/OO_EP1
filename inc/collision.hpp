//Classe utilizada para detectar e resolver colisões entre caracteres
//importantes ao jogo

#ifndef COLLISION_HPP
#define COLLISION_HPP

#include "gameobject.hpp"

class Collision : public GameObject {
private:
	char cursor_object;
public:
	Collision();
	~Collision();
	bool detectCollision(WINDOW* current_win, int pos_y, int a, int pos_x, int b, char object);
};

#endif