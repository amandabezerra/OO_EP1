//Classe utilizada para desenhar um objeto na tela

#ifndef DRAW_HPP
#define DRAW_HPP

#include "gameobject.hpp"

class Draw : public GameObject{

public:

	Draw();
	~Draw();
	void setPosDrawX(int pos_x);
	int getPosDrawX();
	
	void setPosDrawY(int pos_y);
	int getPosDrawY();
	
	void setDraw(char sprite);
	void setDraw(WINDOW * win, int pos_y, int pos_x, char sprite);
	char getDraw();
};

#endif