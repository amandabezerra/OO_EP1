//Classe utilizada para criar aleatoriamente caracteres na tela
//que ao serem capturados incrementam o score do jogador

#ifndef BONUS_HPP
#define BONUS_HPP

#include "gameobject.hpp"

class Bonus : public GameObject{

public:
	Bonus();
	~Bonus();

	void displayBonus(WINDOW* win);	
};

#endif