#include <iostream>
#include "collision.hpp"

using namespace std;

Collision::Collision(){}
Collision::~Collision(){}

bool Collision::detectCollision(WINDOW* current_win, int pos_y, int a, int pos_x, int b, char object){
	wmove(current_win, pos_y - a, pos_x - b);
	wrefresh(current_win);
	char c = winch(current_win);
	mvwaddch(current_win, pos_y, pos_x, c);
	if(c == object){
		return true;
	}else{
		return false;
	}
}