#include "map.hpp"
#include "gameobject.hpp"
#include "draw.hpp"
#include <iostream>
#include <fstream>

using namespace std;

Map::Map(){}
Map::~Map(){}

void Map::displayMap(WINDOW* current_win){

	int i, j;
	char aux;
	
	Draw map;
	ifstream mapfile("doc/map.txt", ios::binary);
	if(mapfile.is_open()){
		for(i = 0; i < 20; i++){
			for(j = 0; j < 50; j++){
				mapfile >> aux;
				map.setPosDrawY(i);
				map.setPosDrawX(j);
				map.setDraw(current_win, map.getPosDrawY(), map.getPosDrawX(), aux);
			}
		}
	}else{
		wprintw(current_win, "Mapa não localizado");
	}
	mapfile.close();
	wrefresh(current_win);
}
void Map::displayMenu(WINDOW* current_win){

	int i, j;
	char aux;
	
	Draw menu;
	ifstream menufile("doc/menu.txt", ios::binary);
	if(menufile.is_open()){
		for(i = 0; i < 20; i++){
			for(j = 0; j < 50; j++){
				menufile >> aux;
				menu.setPosDrawY(i);
				menu.setPosDrawX(j);
				menu.setDraw(current_win, menu.getPosDrawY(), menu.getPosDrawX(), aux);
			}
		}
	}else{
		wprintw(current_win, "Menu não localizado");
	}
	menufile.close();
	wrefresh(current_win);
}
