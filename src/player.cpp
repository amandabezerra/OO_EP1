#include "gameobject.hpp"
#include "draw.hpp"
#include "player.hpp"
#include "collision.hpp"
#include <iostream>
#include <ncurses.h>

using namespace std;

Player::Player(WINDOW* win, int y, int x, char c){
	current_win = win;
	life = 5;
	score = 0;
	pos_x = x;
	pos_y = y;
	keypad(current_win, true);
	sprite = c;
	alive = true;
	winner = false;
}
Player::~Player(){}

bool Player::gameWinner(){
	return winner;
}
bool Player::gameOver(){
	return alive;
}

Collision player;

void Player::moveUp(){
	if(player.detectCollision(current_win, pos_y, 1, pos_x, 0, '=') == false){
		pos_y--;
	}
	if(player.detectCollision(current_win, pos_y, 0, pos_x, 0, '*') == true){
		life--;
		if(life == 0){
			alive = false;
			gameOver();
		}
		mvwaddch(current_win, pos_y + 1, pos_x, '-');
	}
	if(player.detectCollision(current_win, pos_y, 0, pos_x, 0, '$') == true){
		score++;
		mvwaddch(current_win, pos_y + 1, pos_x, '-');
	}
}
void Player::moveDown(){
	if(player.detectCollision(current_win, pos_y, -1, pos_x, 0, '=') == false){
		pos_y++;
	}
	if(player.detectCollision(current_win, pos_y, 0, pos_x, 0, '*') == true){
		life--;
		if(life == 0){
			alive = false;
			gameOver();
		}
		mvwaddch(current_win, pos_y - 1, pos_x, '-');
	}
	if(player.detectCollision(current_win, pos_y, 0, pos_x, 0, '$') == true){
		score++;
		mvwaddch(current_win, pos_y - 1, pos_x, '-');
	}
}
void Player::moveLeft(){
	if(player.detectCollision(current_win, pos_y, 0, pos_x, 1, '=') == false){
		pos_x--;
	}
	if(player.detectCollision(current_win, pos_y, 0, pos_x, 0, '*') == true){
		life--;
		if(life == 0){
			alive = false;
			gameOver();
		}
		mvwaddch(current_win, pos_y, pos_x + 1, '-');
	}
	if(player.detectCollision(current_win, pos_y, 0, pos_x, 0, '$') == true){
		score++;
		mvwaddch(current_win, pos_y, pos_x + 1, '-');
	}
}
void Player::moveRight(){
	char c = winch(current_win);
	mvwaddch(current_win, pos_y, pos_x, c);
	if(player.detectCollision(current_win, pos_y, 0, pos_x, 0, '=') == false){
		pos_x++;
	}
	if(player.detectCollision(current_win, pos_y, 0, pos_x, 0, '*') == true){
		life--;
		if(life == 0){
			alive = false;
			gameOver();
		}
		mvwaddch(current_win, pos_y, pos_x - 1, '-');
	}
	if(player.detectCollision(current_win, pos_y, 0, pos_x, 0, '$') == true){
		score++;
		mvwaddch(current_win, pos_y, pos_x - 1, '-');
	}
	if(player.detectCollision(current_win, pos_y, 0, pos_x, 0, '8') == true){
		winner = true;
	}
}
int Player::getMove(){
	int choice = wgetch(current_win);
	switch (choice){
		case KEY_UP:
			moveUp();
			break;
		case KEY_DOWN:
			moveDown();
			break;
		case KEY_LEFT:
			moveLeft();
			break;
		case KEY_RIGHT:
			moveRight();
			break;
		default:
			break;
	}
	return choice;
}

void Player::setDraw(){
	mvwaddch(current_win, pos_y, pos_x, sprite);
}

void Player::displayInfos(){
	mvwprintw(current_win, 22, 0, "Lifes: %d", life);
	mvwprintw(current_win, 23, 0, "Score: %d", score);
}