#include "trap.hpp"
#include "gameobject.hpp"
#include "collision.hpp"
#include <iostream>
#include <stdlib.h>

using namespace std;

Trap::Trap(WINDOW* win){
	current_win = win;
	sprite = '*';
}
Trap::~Trap(){}

Collision trap;

void Trap::displayTraps(){
	int i;
	for(i = 0; i < 19; i++){
		pos_y = 1 + rand() % 18;
		pos_x = 1 + rand() % 48;
		if(trap.detectCollision(current_win, pos_y, 0, pos_x, 0, '=') == false 
			&& trap.detectCollision(current_win, pos_y, 0, pos_x, 0, '#') == false){
			mvwaddch(current_win, pos_y, pos_x, sprite);
		}
	}
}
void Trap::destroyTraps(){
	int i, j;
	for(i = 0; i <= 20; i++){
		for(j = 0; j<= 50; j++){
			char c = mvwinch(current_win, i, j);
			if(c == '*'){
				mvwaddch(current_win, i, j, '-');
			}
		}
	}
}

