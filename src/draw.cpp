#include "gameobject.hpp"
#include "draw.hpp"
#include <iostream>

using namespace std;

Draw::Draw(){
	pos_x = 0;
	pos_y = 0;
	sprite = ' ';
}
Draw::~Draw(){}
void Draw::setPosDrawX(int pos_x){
	this->pos_x = pos_x;
}
int Draw::getPosDrawX(){
	return pos_x;
}

void Draw::setPosDrawY(int pos_y){
	this->pos_y = pos_y;
}
int Draw::getPosDrawY(){
	return pos_y;
}

void Draw::setDraw(char sprite){
	this->sprite = sprite;
}
void Draw::setDraw(WINDOW * win, int pos_y, int pos_x, char sprite){
	this->sprite = sprite;
	mvwaddch(win, pos_y, pos_x, sprite);
}
char Draw::getDraw(){
	return sprite;
}