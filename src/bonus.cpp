#include "bonus.hpp"
#include "collision.hpp"
#include <iostream>
#include <stdlib.h>

using namespace std;

Bonus::Bonus(){
	sprite = '$';
}
Bonus::~Bonus(){}

Collision bonus;

void Bonus::displayBonus(WINDOW* current_win){
	int i;
	for(i = 0; i < 11; i++){
		pos_y = 1 + rand() % 18;
		pos_x = 1 + rand() % 48;
		if(bonus.detectCollision(current_win, pos_y, 0, pos_x, 0, '=') == false 
			&& bonus.detectCollision(current_win, pos_y, 0, pos_x, 0, '#') == false){
			mvwaddch(current_win, pos_y, pos_x, sprite);
		}
	}
}