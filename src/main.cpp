#include "gameobject.hpp"
#include "map.hpp"
#include "player.hpp"
#include "trap.hpp"
#include "bonus.hpp"
#include <iostream>
#include <fstream>
#include <ncurses.h>
#include <stdlib.h>

using namespace std;

int main(int argc, char ** argv){

	initscr();
	clear();
	noecho();

	//Definição de parâmetros e criação da janela do jogo
	int height = 30, widht = 50, start_y = 0, start_x = 0;
	WINDOW* game_window = newwin(height, widht, start_y, start_x);
	keypad(game_window, TRUE);
	
	//Definição de parâmetros e criação do menu do jogo
	int i, option = 0, choice;
	string options[3] = {"Start new game", "Ranking", "Credits"};
	Map game_menu;
	game_menu.displayMenu(game_window);
	
	//Loop utilizado para manter o menu do jogo	
	while(1){
		
		//Implementação de lógica para manter em destaque as opções do menu
		for(i = 0; i < 3; i++){
			if(i == option){
				wattron(game_window, A_REVERSE);
			}
			mvwprintw(game_window, i + 8, 7, options[i].c_str());
			wattroff(game_window, A_REVERSE);
		}

		//Captura da opção selecionada pelo jogador no menu
		choice = wgetch(game_window);

		//Implementação da lógica de movimentos de seleção no menu
		switch(choice){
			case KEY_UP:
				option--;
				if(option == -1){
					option = 0;
				}
				break;
			case KEY_DOWN:
				option++;
				if(option == 3){
					option = 2;
				}
				break;
			default:
				break;
		}

		//Implementação de lógica de tomada de decisão caso o usuário uma opção seja selecionada com ENTER
		if(choice == 10){
			if(option == 0){
				
				//Definição de parâmetros do jogo
				int count = 6;
				Map* game_map = new Map();
				Player* game_player = new Player(game_window, 1, 1, '#');			
				Trap* game_trap = new Trap(game_window);
				Bonus* game_bonus = new Bonus();
				wclear(game_window);
				game_map->displayMap(game_window);
				game_bonus->displayBonus(game_window);

				//Loop utilizado para manter o jogo
				do{
					
					//Constrói e destrói armadilhas a cada 7 movimentações do jogador
					count++;
					if(count%7 == 0){
						game_trap->destroyTraps();
						game_trap->displayTraps();
					}

					//Mantém informações do jogo na tela
					game_player->displayInfos();
					
					//Mantem o jogador na tela
					game_player->setDraw();

					//Condição de parada em caso de derrota
					if(game_player->gameOver()== false){
						break;
					}

					//Condição de parada em caso de vitória
					if(game_player->gameWinner() == true){
						break;
					}

					wrefresh(game_window);
					
				}while(game_player->getMove() != ' ');
				wclear(game_window);
				wrefresh(game_window);

				//Verifica e informa os casos de derrota e vitória
				if(game_player->gameOver()== false){
					mvwprintw(game_window, 0, 0, "GAME OVER!");
					mvwprintw(game_window, 1, 0, "Press any key to go back to menu");
					wgetch(game_window);
				}
				if(game_player->gameWinner() == true){
					mvwprintw(game_window, 0, 0, "YOU WIN!");
					mvwprintw(game_window, 1, 0, "Press any key to go back to menu");
					wgetch(game_window);
				}

				delete(game_player);
				delete(game_bonus);
				delete(game_trap);

				wclear(game_window);
				wrefresh(game_window);
			}

			//Opção de exibição de ranking
			if(option == 1){
				wclear(game_window);
				wrefresh(game_window);
				game_menu.displayMenu(game_window);
				mvwprintw(game_window, 8, 33, "Ranking: ");
				
			}
		
			//Opção de exibição de créditos
			if(option == 2){
				wclear(game_window);
				game_menu.displayMenu(game_window);
				mvwprintw(game_window, 8, 33, "Credits: ");
				mvwprintw(game_window, 9, 33, "UnB Gama");
				mvwprintw(game_window, 10, 33, "Disciplina OO");
				mvwprintw(game_window, 11, 33, "Projeto 1");
				mvwprintw(game_window, 12, 33, "Amanda Bezerra");
				wrefresh(game_window);				
			}
		}

		//Opção para voltar ao menu após vencer ou perder jogo
		if(choice == ' ' || choice == ' '){
			wclear(game_window);
			wrefresh(game_window);
			game_menu.displayMenu(game_window);
		}

		//Opção de saída de menu
		if(choice == 'x' || choice == 'X'){
			break;
		}
	}
	endwin();
	return 0;
}
