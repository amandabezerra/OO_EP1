### Orientação a Objetos EP1 GAME
Amanda Bezerra da Silva - 15/0057113

Este projeto consiste em um jogo de labirinto criado a partir da biblioteca Ncurses e utilizando a linguagem C++. Seu objetivo é validar os conhecimentos adquiridos na disciplina Orientação a Objetos, ministrada pelo professor Renato Coral.

#### Instalação e execução
	1. Certifique-se de ter instalado em sua máquina a biblioteca Ncurses. 
       Para mais informações, recomenda-se a seguinte leitura"How To Install ncurses Library on a Linux" (https://www.cyberciti.biz/faq/linux-install-ncurses-library-headers-on-debian-ubuntu-centos-fedora/);
	2. Faça o clone deste projeto com o comando "git clone https://amandabezerra@gitlab.com/amandabezerra/OO_EP1.git";
	3. Através do terminal do Linux, acesse a pasta clonada;
	4. Execute o jogo utilizando o comando "make game".